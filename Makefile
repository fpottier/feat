# ------------------------------------------------------------------------------

# The name of the repository.
THIS     := feat

# The libraries (topologically sorted).
THIS_COMMAS := feat-core,feat,feat-num
THIS_SPACES := feat-core feat feat-num

# The version number is automatically set to the current date,
# unless DATE is defined on the command line.
DATE     := $(shell /bin/date +%Y%m%d)

# The repository URL (https).
REPO     := https://gitlab.inria.fr/fpottier/$(THIS)

# The archive URL (https).
ARCHIVE  := $(REPO)/-/archive/$(DATE)/archive.tar.gz

# ------------------------------------------------------------------------------

.PHONY: all
all:
	@ dune build @all

.PHONY: install
install:
	@ dune build -p $(THIS_COMMAS)
	@ dune install -p $(THIS_COMMAS)

.PHONY: clean
clean:
	@ rm -f dune-workspace.versions
	@ $(FIND) . -name "*~" | xargs rm -f
	@ dune clean

.PHONY: test
test:
	@ dune runtest

.PHONY: uninstall
uninstall:
	@ for t in $(THIS_SPACES) ; do ocamlfind remove $$t; done || true

.PHONY: reinstall
reinstall: uninstall
	@ make install

.PHONY: show
show: reinstall
	@ echo "#require \"feat-core\";;\n#require \"feat\";;\n#require \"feat-num\";;\n#show FeatCore;;\n#show Feat;;\n#show FeatNum;;" | ocaml

.PHONY: pin
pin:
	@ for d in $(THIS_SPACES) ; do opam pin --yes add $$d . ; done

.PHONY: unpin
unpin:
	@ opam pin --yes remove $(THIS_SPACES)

HEADACHE := headache
LIBHEAD  := $(shell pwd)/headers/library-header
FIND     := $(shell if command -v gfind >/dev/null ; then echo gfind ; else echo find ; fi)

.PHONY: headache
headache:
	@ for d in $(THIS_SPACES) demo ; do \
	    $(FIND) $$d -regex ".*\.ml\(i\|y\|l\)?" \
	    -exec $(HEADACHE) -h $(LIBHEAD) "{}" ";" ; \
	  done

.PHONY: release
release:
# Make sure the current version can be compiled and installed.
	@ make uninstall
	@ make clean
	@ make install
	@ make uninstall
# Check the current package description.
	@ opam lint
# Check if everything has been committed.
	@ if [ -n "$$(git status --porcelain)" ] ; then \
	    echo "Error: there remain uncommitted changes." ; \
	    git status ; \
	    exit 1 ; \
	  else \
	    echo "Now making a release..." ; \
	  fi
# Create a git tag.
	@ git tag -a $(DATE) -m "Release $(DATE)."
# Upload. (This automatically makes a .tar.gz archive available on gitlab.)
	@ git push
	@ git push --tags
# Done.
	@ echo "Done."
	@ echo "If happy, please type:"
	@ echo "  \"make publish\"   to publish a new opam package"
	@ echo "  \"make export\"    to upload the documentation to yquem.inria.fr"

.PHONY: publish
publish:
# Publish an opam description.
	@ opam publish -v $(DATE) $(THIS_SPACES) $(ARCHIVE)

# ------------------------------------------------------------------------------

# Documentation.

DOCDIR = _build/default/_doc/_html
DOC    = $(DOCDIR)/index.html

.PHONY: doc
doc:
	@ rm -rf _build/default/_doc
	@ dune clean
	@ dune build @doc
	@ echo "You can view the documentation by typing 'make view'".

.PHONY: view
view: doc
	@ echo Attempting to open $(DOC)...
	@ if command -v firefox > /dev/null ; then \
	  firefox $(DOC) ; \
	else \
	  open -a /Applications/Firefox.app/ $(DOC) ; \
	fi

.PHONY: export
export: doc
	ssh yquem.inria.fr rm -rf public_html/$(THIS)/doc
	scp -r $(DOCDIR) yquem.inria.fr:public_html/$(THIS)/doc

# ------------------------------------------------------------------------------

# [make versions] compiles and tests under many versions of OCaml,
# whose list is specified below.

VERSIONS := \
  4.03.0 \
  4.04.2 \
  4.05.0 \
  4.06.1 \
  4.07.1 \
  4.08.1 \
  4.09.1 \
  4.09.0+bytecode-only \
  4.10.0 \
  4.11.1 \
  4.12.0 \
  4.13.1 \
  4.14.2 \
  5.0.0  \
  5.1.0  \
  5.2.0  \

.PHONY: switches
switches:
	@ for v in $(VERSIONS) ; do \
	    opam switch create $$v || \
	    opam install --switch $$v --yes zarith seq fix num; \
	  done

.PHONY: versions
versions:
	@(echo "(lang dune 2.0)" && \
	  for v in $(VERSIONS) ; do \
	    echo "(context (opam (switch $$v)))" ; \
	  done) > dune-workspace.versions
	@ dune build --workspace dune-workspace.versions @install

.PHONY: handiwork
handiwork:
	@ current=`opam switch show` ; \
	  for v in $(VERSIONS) ; do \
	    opam switch $$v && \
	    eval $$(opam env --switch=$$v) && \
	    opam install --yes zarith seq fix.20201120 num; \
	  done ; \
	  opam switch $$current
