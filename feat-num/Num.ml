(******************************************************************************)
(*                                                                            *)
(*                                     Feat                                   *)
(*                                                                            *)
(*                        François Pottier, Inria Paris                       *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT license, as described in the file LICENSE.               *)
(******************************************************************************)

open Big_int

type t = big_int
let zero = zero_big_int
let one = big_int_of_int 1
let pred = pred_big_int
let add = add_big_int
let sub = sub_big_int
let mul = mult_big_int
let div_rem = quomod_big_int
let equal = eq_big_int
let lt = lt_big_int
let of_int = big_int_of_int
exception Overflow
let to_int = try int_of_big_int with Failure _ -> raise Overflow
let leq = le_big_int
let geq = ge_big_int
let (/) = div_big_int
let ( * ) = mult_big_int
let (mod) = mod_big_int
let (lsl) = shift_left_big_int
let (lor) = or_big_int
let to_string = string_of_big_int
