(******************************************************************************)
(*                                                                            *)
(*                                     Feat                                   *)
(*                                                                            *)
(*                        François Pottier, Inria Paris                       *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT license, as described in the file LICENSE.               *)
(******************************************************************************)

(* This module satisfies the signature [BigIntSig.EXTENDED], instantiated with
   unbounded integers provided by the library [num]. *)
include FeatCore.BigIntSig.EXTENDED with type t = Big_int.big_int
