# CHANGES

## 2022/04/07

* Fix a serious bug in `Enum.sample`, which used OCaml's random number
  generator in such a way that it would always produce the same number. The
  fix modifies the signature of several internal modules, such as
  `Feat.RandomBigInt`. The public API of the libraries `feat` and `feat-num`
  is unaffected. (Reported by Guyslain Naves.)

## 2022/01/01

* Improved documentation.

## 2021/12/24

* The library is now split in three packages: `feat-core`, `feat`, and
  `feat-num`. `feat-core` is parameterized over an implementation of big
  integers and over a random number generator. `feat` instantiates `feat-core`
  with big integers from the library `zarith` and with OCaml's standard random
  number generator. `feat-num` instantiates `feat-core` with big integers from
  the library `num` and with OCaml's standard random number generator. The
  packages `feat` and `feat-num` offer the same API and are interchangeable.
  (See `demo/with-feat` and `demo/with-feat-num` to see how the same code can
  be linked with either package.) The three submodules offered both by `feat`
  and by `feat-num` are `Num`, `IFSeq`, and `Enum`. Compatibility with the
  previous release of `feat` should be complete as long as only the submodules
  `IFSeq` and `Enum` were used. (Contributed by Jonah Beckford; reviewed and
  adapted by François Pottier.)

* Avoid inadvertent uses of OCaml's polymorphic comparison operators on big
  integers.

* Improved documentation.

## 2020/12/31

* Initial release.
