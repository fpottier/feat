## TO DO

* We probably want a lazy variant of the combinator `**`, at the level
  of sequences and/or enumerations, so that if the left-hand argument
  produces an empty sequence, then the right-hand argument is not
  evaluated.

* The operator `(**)` has bad complexity, as it enumerates all possible
  ways of splitting the current size. This is especially bad if one
  operand has impossible sizes, e.g., if one computes `e1 ** e2`
  where the enumeration `e1` is empty below or above a certain size.
  Document this issue?
  To solve this issue, one might wish to precompute an abstract interpretation
  of the possible size (e.g., lower and upper bounds)
  of the inhabitants of an enumeration; but that would require
  changing the type `enum` and might make it more difficult
  to define `fix`.

* Write some documentation.

* Test. Check that `length`, `get`, `foreach` and `to_seq` are consistent.

* Test. Check that `foreach` is faster than iterated `get`.

* Try implementing a balancing scheme for `Sum`?

* Define a combinator `smap` where the user function `f` has access to the
  size of the data.
