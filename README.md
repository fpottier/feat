## Feat

Feat is an OCaml library that offers support for counting, enumerating, and
sampling objects of a certain kind, such as (say) the inhabitants of an
algebraic data type.

## Documentation

See the [documentation of the latest released
version](http://cambium.inria.fr/~fpottier/feat/doc/feat/).

The external library
[Fix](https://gitlab.inria.fr/fpottier/fix/)
comes in handy when building enumerations
of recursive algebraic data types:
see the [demo](demo/Test.ml).
