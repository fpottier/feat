(******************************************************************************)
(*                                                                            *)
(*                                     Feat                                   *)
(*                                                                            *)
(*                        François Pottier, Inria Paris                       *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT license, as described in the file LICENSE.               *)
(******************************************************************************)

(**This module provides an abstract data type of {b implicit, finite
   sequences}. An implicit sequence is not explicitly represented in memory
   as an actual sequence of elements: instead, it is {e described} by a data
   structure which contains enough information to produce an arbitrary
   element upon request. This design decision imposes some constraints on
   the operations that can be efficiently supported: for instance, [filter]
   is not supported. *)

include FeatCore.IFSeqSig.IFSEQ_EXTENDED with type index = Num.t
