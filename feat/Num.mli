(******************************************************************************)
(*                                                                            *)
(*                                     Feat                                   *)
(*                                                                            *)
(*                        François Pottier, Inria Paris                       *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT license, as described in the file LICENSE.               *)
(******************************************************************************)

(**This module implements a small set of operations on big integers. *)

(* This module satisfies the signature [BigIntSig.EXTENDED],
   instantiated with unbounded integers from [zarith]. *)
include FeatCore.BigIntSig.EXTENDED with type t = Z.t
