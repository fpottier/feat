(******************************************************************************)
(*                                                                            *)
(*                                     Feat                                   *)
(*                                                                            *)
(*                        François Pottier, Inria Paris                       *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT license, as described in the file LICENSE.               *)
(******************************************************************************)

(**This module offers a concrete data type ['a enum] of {b enumerations} of
   elements of type ['a]. Suppose that every element of type ['a] is
   implicitly assigned a certain size. Then, an enumeration is a function of
   an integer [s] to the (implicit, finite) sequence of all elements whose
   size is [s]. *)

include FeatCore.EnumSig.ENUM with module IFSeq = IFSeq
