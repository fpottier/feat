(******************************************************************************)
(*                                                                            *)
(*                                     Feat                                   *)
(*                                                                            *)
(*                        François Pottier, Inria Paris                       *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT license, as described in the file LICENSE.               *)
(******************************************************************************)

(** A subset of the signature of the module {!Stdlib.Random}. *)
module type S = sig

  (** [bits s] returns 30 random bits in a nonnegative integer. *)
  val bits : unit -> int

  (** [int bound] returns a random integer comprised between 0 (inclusive)
      and [bound] (exclusive). [bound] must be greater than 0 and less than
      2{^30}. *)
  val int : int -> int

end
