(******************************************************************************)
(*                                                                            *)
(*                                     Feat                                   *)
(*                                                                            *)
(*                        François Pottier, Inria Paris                       *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT license, as described in the file LICENSE.               *)
(******************************************************************************)

(**A signature for big numbers that is satisfied both by [Zarith] and
   by the older, slower but license-friendly [Num] package. This minimal
   signature is required by {!IFSeqSyn} and {!IFSeqObj}. *)
module type BASIC = sig
  type t
  val zero : t
  val one : t
  val pred : t -> t
  val add : t -> t -> t
  val sub : t -> t -> t
  val mul : t -> t -> t
  val div_rem : t -> t -> t * t
  val equal : t -> t -> bool
  val lt : t -> t -> bool
  val of_int : int -> t
  exception Overflow
  val to_int : t -> int
end

(**An extended signature for big numbers, providing more functionality. This
   signature is required by {!RandomBigInt} and by the modules that depend
   on it, such as {!IFSeq}. *)
module type EXTENDED = sig
  include BASIC
  val leq: t -> t -> bool
  val geq: t -> t -> bool
  val (/): t -> t -> t
  val ( * ): t -> t -> t
  val (mod): t -> t -> t
  val (lsl): t -> int -> t
  val (lor): t -> t -> t
  val to_string: t -> string
end
