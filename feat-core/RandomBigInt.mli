(******************************************************************************)
(*                                                                            *)
(*                                     Feat                                   *)
(*                                                                            *)
(*                        François Pottier, Inria Paris                       *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT license, as described in the file LICENSE.               *)
(******************************************************************************)

(* Uniform random generation of large integers. *)

module Make (Z : BigIntSig.EXTENDED) (R : RandomSig.S) : sig
  (**[random range] generates a random number comprised between 0 (included)
     and [range] (excluded). [range] must be positive. *)
  val random: Z.t -> Z.t
end
