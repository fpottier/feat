(******************************************************************************)
(*                                                                            *)
(*                                     Feat                                   *)
(*                                                                            *)
(*                        François Pottier, Inria Paris                       *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT license, as described in the file LICENSE.               *)
(******************************************************************************)

open IFSeqSig

(**[Make] constructs an implementation of the signature
   {!IFSeqSig.IFSEQ_EXTENDED}. It is parameterized over an implementation of
   big integers and a random number generator. *)
module Make
  (Z : BigIntSig.EXTENDED)
  (R : RandomSig.S)
: IFSEQ_EXTENDED with type index = Z.t
